import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Post} from './post';

@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<Post>();
  @Output() editEvent = new EventEmitter<Post>();

  post:Post;
  author1:string;
  content1:string;
  title1:string;
  isEdit:Boolean =false;
  editButtonText = 'Edit';
  cancelButtonText = 'Cancel';

  constructor() { }
  sendCancel(){
    this.post.author = this.author1;
    this.post.content = this.content1;
    this.post.title = this.title1;
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = 'Save': this.editButtonText = 'Edit';
  }

  sendDelete(){
    this.deleteEvent.emit(this.post);
  }

  toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = 'Save': this.editButtonText = 'Edit';
    if(!this.isEdit){
      this.editEvent.emit(this.post); 
    }
  }

  ngOnInit() {
    this.author1 = this.post.author;
    this.content1 = this.post.content;
    this.title1 = this.post.title;
  }

}
