import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
 styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class UsersComponent implements OnInit {

   users;
   isLoading = true;
  currentUser;
  
 select(user){
		this.currentUser = user; 
    console.log(	this.currentUser);
 }
 deleteUser(user){
   this._userService.deleteUser(user);
 }
 addUser(user){
  //  this.users.push(user)
  this._userService.addUser(user);
 }
 updateUser(user){
   this._userService.updateUser(user);
 }

   constructor(private _userService:UsersService) {}

  ngOnInit() {
    // this._userService.getUsers().subscribe(usersData =>
    // {this.users = usersData; this.isLoading = false;}); //like function(userData){this.usres=usersDats}
    this._userService.getUsersFromApi().subscribe(userData => 
    {this.users = userData.result;
      this.isLoading = false;});
  }
}
