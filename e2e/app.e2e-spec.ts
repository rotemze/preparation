import { PreparationPage } from './app.po';

describe('preparation App', function() {
  let page: PreparationPage;

  beforeEach(() => {
    page = new PreparationPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
